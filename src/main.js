import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'

import './scss/iconfont.css'

Vue.config.productionTip = false

// TODO 可能不需要这个方法
Vue.prototype.$goRoute = function (index) {
  this.$router.push(index)
}

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app')
