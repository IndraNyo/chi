import Vue from 'vue'
import Router from 'vue-router'
import login from '@/pages/login'
import register from '@/pages/register'
import main from '@/pages/main'
import user from '@/pages/user'
import profile from '@/pages/profile'
import addRecipe from '@/pages/addRecipe'
import checkRecipe from '@/pages/checkRecipe'
import shoppingList from '@/pages/shoppingList'
import userRegister from '@/components/userRegister'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [{
      path: '/',
      name: 'login',
      component: login
    },
    {
      path: '/main',
      name: 'main',
      component: main
    },
    {
      path: '/login',
      name: 'login',
      component: login
    },
    {
      path: '/user',
      name: 'edit profile',
      component: user
    },
    {
      path: '/profile',
      name: 'profile',
      component: profile
    },
    {
      path: '/checkRecipe',
      name: 'checkRecipe',
      component: checkRecipe
    },
    {
      path: '/shoppingList',
      name: 'shoppingList',
      component: shoppingList
    },
    {
      path: '/addRecipe',
      name: 'addRecipe',
      component: addRecipe
    },
    {
      path: '/register',
      name: 'register',
      component: register,
      children: [{
        path: '/userRegister',
        name: 'userRegister',
        component: userRegister
      }]
    }
    // {
    // path: '/about',
    // name: 'about',
    // route level code-splitting
    // this generates a separate chunk (about.[hash].js) for this route
    // which is lazy-loaded when the route is visited.
    //   component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    // }
  ]
})
